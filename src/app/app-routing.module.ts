import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path:'index',
    loadChildren: ()=>import('../app/main-home/main-home.module')
    .then(m=>m.MainHomeModule)
  },
  {
    path:'catalogo',
    loadChildren: ()=>import('../app/catalogo/catalogo.module')
    .then(m=>m.CatalogoModule)
  },
  {
    path:'',
    redirectTo:'index',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
