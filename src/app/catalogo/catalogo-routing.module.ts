import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaincatalogoComponent } from './maincatalogo/maincatalogo.component';



const routes: Routes = [
  {
    path:'',
    component:MaincatalogoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogoRoutingModule { }
