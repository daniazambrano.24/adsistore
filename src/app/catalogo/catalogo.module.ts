import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogoRoutingModule } from './catalogo-routing.module';
import { MaincatalogoComponent } from './maincatalogo/maincatalogo.component';
import { ModalSuscriptionComponent } from './modal-suscription/modal-suscription.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons'
import {faAdobe} from '@fortawesome/free-brands-svg-icons' 


@NgModule({
  declarations: [MaincatalogoComponent, ModalSuscriptionComponent],
  imports: [
    CommonModule,
    CatalogoRoutingModule,
    FontAwesomeModule
  ]
})
export class CatalogoModule {
  constructor(Library:FaIconLibrary){
    Library.addIcons(faChevronDown);
    Library.addIcons(faChevronUp);
    Library.addIcons(faAdobe);
  }
}
