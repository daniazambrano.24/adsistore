import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaincatalogoComponent } from './maincatalogo.component';

describe('MaincatalogoComponent', () => {
  let component: MaincatalogoComponent;
  let fixture: ComponentFixture<MaincatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaincatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaincatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
