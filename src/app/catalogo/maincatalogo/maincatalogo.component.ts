import { Component, OnInit } from '@angular/core';
import {NgbActiveModal, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { ModalSuscriptionComponent } from '../modal-suscription/modal-suscription.component';

@Component({
  selector: 'app-maincatalogo',
  templateUrl: './maincatalogo.component.html',
  styleUrls: ['./maincatalogo.component.styl'],
  providers:[NgbModalConfig, NgbModal]
})
export class MaincatalogoComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { 
    config.backdrop = 'static';
    config.keyboard = false;
  }
  open(){
    const modalRef = this.modalService.open(ModalSuscriptionComponent);
  }

  ngOnInit(): void {
    this.open();
  }

}
