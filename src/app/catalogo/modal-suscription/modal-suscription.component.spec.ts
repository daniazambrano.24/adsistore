import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSuscriptionComponent } from './modal-suscription.component';

describe('ModalSuscriptionComponent', () => {
  let component: ModalSuscriptionComponent;
  let fixture: ComponentFixture<ModalSuscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSuscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSuscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
