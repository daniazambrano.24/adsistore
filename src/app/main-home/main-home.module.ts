import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainHomeRoutingModule } from './main-home-routing.module';
import { SlideComponent } from './slide/slide.component';
import { IndexComponent } from './index/index.component';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons'
import {faAdobe} from '@fortawesome/free-brands-svg-icons' 


@NgModule({
  declarations: [SlideComponent, IndexComponent],
  imports: [
    CommonModule,
    MainHomeRoutingModule,
    FontAwesomeModule
  ]
})
export class MainHomeModule {
  constructor(Library:FaIconLibrary){
    Library.addIcons(faChevronDown);
  }
}
